package agenda;

import java.util.ArrayList;

public class ControlePessoa{

//atributos

    private ArrayList<Pessoa> listaPessoas;

//construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }

// métodos
  
    public ArrayList<Pessoa> getListaPessoas() {
        return listaPessoas;
    }
        public void adicionar(Pessoa umaPessoa) {
            
	    listaPessoas.add(umaPessoa);
        }

    
        public void remover(Pessoa umaPessoa) {
            
                listaPessoas.remove(umaPessoa);
            }
           
        
    

        public Pessoa pesquisar(String nome) {
            for (Pessoa p: listaPessoas) {
                if (p.getNome().equalsIgnoreCase(nome)) return p;
            }
            return null;
        }    

    
}
