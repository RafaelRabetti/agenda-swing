package agenda;

import java.awt.Menu;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class TelaAgenda extends javax.swing.JFrame {

   ControlePessoa umControlePessoa = new ControlePessoa();
   private Pessoa umaPessoa;
   private boolean novoContato;
   private boolean modoAlteracao;
   
    public TelaAgenda() {
        initComponents();
        this.habilitarDesabilitarCampos();
        this.TabelaPessoas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    TelaAgenda(Menu aThis, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void limparCampos() {
        NomeT.setText("");
        EmailT.setText("");
        TelefoneT.setText("");
    }
    
    private void preencherCampos() {
        NomeT.setText(umaPessoa.getNome());
        EmailT.setText(umaPessoa.getEmail());
        TelefoneT.setText(umaPessoa.getTelefone());
    }
    
    private boolean validarCampos() {
        if (NomeT.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Nome' não foi informado.");
            NomeT.requestFocus();
            return false;
        }
        
        if (EmailT.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Email' não foi informado.");
            EmailT.requestFocus();
            return false;
        }
        
        if (TelefoneT.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Telefone' não foi informado.");
            TelefoneT.requestFocus();
            return false;
        }
       return true;
    }
    
    private void habilitarDesabilitarCampos() {
        NomeT.setEnabled(modoAlteracao);
        EmailT.setEnabled(modoAlteracao);
        TelefoneT.setEnabled(modoAlteracao);
    }
    
    private void carregarListaPessoas() {
        ArrayList<Pessoa> listaPessoas = umControlePessoa.getListaPessoas();
        DefaultTableModel model = (DefaultTableModel) TabelaPessoas.getModel();
        model.setRowCount(0);
        for (Pessoa p : listaPessoas) {
            model.addRow(new String[]{p.getNome(), p.getEmail()});
        }
        TabelaPessoas.setModel(model);
    }
    
     private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        PainelAgenda = new javax.swing.JPanel();
        Nome = new javax.swing.JLabel();
        NomeT = new javax.swing.JTextField();
        Email = new javax.swing.JLabel();
        Telefone = new javax.swing.JLabel();
        EmailT = new javax.swing.JTextField();
        TelefoneT = new javax.swing.JTextField();
        Salvar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        TabelaPessoas = new javax.swing.JTable();
        Remover = new javax.swing.JButton();
        Novo = new javax.swing.JButton();
        Alterar = new javax.swing.JButton();
        Pesquisar = new javax.swing.JButton();
        Cancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Nome.setText("Nome:");

        Email.setText("Email:");

        Telefone.setText("Telefone:");

        EmailT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EmailTActionPerformed(evt);
            }
        });

        TelefoneT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TelefoneTActionPerformed(evt);
            }
        });

        Salvar.setText("Salvar");
        Salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalvarActionPerformed(evt);
            }
        });

        TabelaPessoas.setModel(new javax.swing.table.DefaultTableModel 
            (
                null,
                new String [] {
                    "Nome", "Email"
                }
            )
            {
                @Override    
                public boolean isCellEditable(int rowIndex, int mColIndex) {
                    return false;
                }
            });
            TabelaPessoas.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    TabelaPessoasMouseClicked(evt);
                }
            });
            jScrollPane1.setViewportView(TabelaPessoas);

            Remover.setText("Remover");
            Remover.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    RemoverActionPerformed(evt);
                }
            });

            Novo.setText("Novo");
            Novo.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    NovoActionPerformed(evt);
                }
            });

            Alterar.setText("Alterar");
            Alterar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    AlterarActionPerformed(evt);
                }
            });

            Pesquisar.setText("Pesquisar");
            Pesquisar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    PesquisarActionPerformed(evt);
                }
            });

            Cancelar.setText("Cancelar");
            Cancelar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    CancelarActionPerformed(evt);
                }
            });

            javax.swing.GroupLayout PainelAgendaLayout = new javax.swing.GroupLayout(PainelAgenda);
            PainelAgenda.setLayout(PainelAgendaLayout);
            PainelAgendaLayout.setHorizontalGroup(
                PainelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PainelAgendaLayout.createSequentialGroup()
                    .addGroup(PainelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(PainelAgendaLayout.createSequentialGroup()
                            .addGroup(PainelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(Telefone)
                                .addComponent(Email)
                                .addComponent(Nome))
                            .addGap(3, 3, 3)
                            .addGroup(PainelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(NomeT, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(EmailT, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(TelefoneT, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(PainelAgendaLayout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 428, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(PainelAgendaLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(Salvar)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(Cancelar))
                        .addGroup(PainelAgendaLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(Novo)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(Alterar)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(Pesquisar)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(Remover)))
                    .addContainerGap(39, Short.MAX_VALUE))
            );
            PainelAgendaLayout.setVerticalGroup(
                PainelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PainelAgendaLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addGroup(PainelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Nome)
                        .addComponent(NomeT, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(PainelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(EmailT, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Email))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(PainelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Telefone)
                        .addComponent(TelefoneT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(PainelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Remover)
                        .addComponent(Novo)
                        .addComponent(Alterar)
                        .addComponent(Pesquisar))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                    .addGroup(PainelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Salvar)
                        .addComponent(Cancelar))
                    .addContainerGap())
            );

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(PainelAgenda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(PainelAgenda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            );

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void TelefoneTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TelefoneTActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TelefoneTActionPerformed

    private void EmailTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EmailTActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EmailTActionPerformed

    private void SalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalvarActionPerformed
        
        if (this.validarCampos() == false) {
            return;
        }
        
        String umNome = this.NomeT.getText();
        
        if (novoContato == true) {
            umaPessoa = new Pessoa(NomeT.getText());
        } else {
            umaPessoa.setNome(NomeT.getText());
        }
        
        umaPessoa.setEmail(EmailT.getText());
        umaPessoa.setTelefone(TelefoneT.getText());
        
        if (novoContato == true) {
            umControlePessoa.adicionar(umaPessoa);
        }
        
        modoAlteracao = false;
        novoContato = false;
        
        this.carregarListaPessoas();
        this.habilitarDesabilitarCampos();       
        
    }//GEN-LAST:event_SalvarActionPerformed

    private void RemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RemoverActionPerformed
        this.umControlePessoa.remover(umaPessoa);
        this.habilitarDesabilitarCampos();
        this.carregarListaPessoas();
    }//GEN-LAST:event_RemoverActionPerformed

    private void TabelaPessoasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabelaPessoasMouseClicked
        if (TabelaPessoas.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) TabelaPessoas.getModel();
            String umNome = (String) model.getValueAt(TabelaPessoas.getSelectedRow(), 0);
            this.pesquisarPessoa(umNome);
        }
    }//GEN-LAST:event_TabelaPessoasMouseClicked

    private void NovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NovoActionPerformed
        umaPessoa = null;
        modoAlteracao = true;
        novoContato = true;
        this.limparCampos();
        this.habilitarDesabilitarCampos();        
        this.NomeT.requestFocus();    
        
    }//GEN-LAST:event_NovoActionPerformed

    private void AlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AlterarActionPerformed
        modoAlteracao = true;
        novoContato = false;
        this.habilitarDesabilitarCampos();
        this.NomeT.requestFocus();
    }//GEN-LAST:event_AlterarActionPerformed

    private void PesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PesquisarActionPerformed
        String pesquisa = JOptionPane.showInputDialog("Informe o nome da Pessoa.");
    if (pesquisa != null) {
        this.pesquisarPessoa(pesquisa);
    }
    }//GEN-LAST:event_PesquisarActionPerformed

    private void CancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelarActionPerformed
        if (novoContato == true) {
            this.limparCampos();
        } else {
            this.preencherCampos();
        }
        modoAlteracao = false;
        novoContato = false;
        this.habilitarDesabilitarCampos();
    }//GEN-LAST:event_CancelarActionPerformed

    private void pesquisarPessoa(String nome) {
        Pessoa pessoaPesquisada = umControlePessoa.pesquisar(nome);

        if (pessoaPesquisada == null) {
            exibirInformacao("Pessoa não encontrada.");
        } else {
            this.umaPessoa = pessoaPesquisada;
            this.preencherCampos();
            this.habilitarDesabilitarCampos();
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaAgenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaAgenda().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Alterar;
    private javax.swing.JButton Cancelar;
    private javax.swing.JLabel Email;
    private javax.swing.JTextField EmailT;
    private javax.swing.JLabel Nome;
    private javax.swing.JTextField NomeT;
    private javax.swing.JButton Novo;
    private javax.swing.JPanel PainelAgenda;
    private javax.swing.JButton Pesquisar;
    private javax.swing.JButton Remover;
    private javax.swing.JButton Salvar;
    private javax.swing.JTable TabelaPessoas;
    private javax.swing.JLabel Telefone;
    private javax.swing.JTextField TelefoneT;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables

    
}
